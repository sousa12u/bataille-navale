/**
 * Classe permettant de lever des exceptions de bateau
 * @author Alexandre Sousa
 *
 */
public class BateauException extends Exception {
	public BateauException(String message) {
		super(message);
	}
}
