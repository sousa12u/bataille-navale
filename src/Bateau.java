import java.io.Serializable;
import java.util.Comparator;

public class Bateau implements Serializable {
	/**
	 * attribut définissant le nom du bateau
	 */
	private String nom;
	/**
	 * attribut définissant la taille du bateau
	 */
	private int taille;
	/**
	 * attribut définissant le taux de destruction du bateau
	 */
	private int destruction = 0;
	/**
	 * attribut définissant si le bateau a été détruit ou non
	 */
	private boolean detruit;

	/**
	 * Constructeur de bateau
	 * @param pnom nom du bateau pour l'identifier
	 * @throws BateauException si le bateau n'a pas été créé pour divers raisons
	 */
	public Bateau(String pnom) throws BateauException {
		if (pnom == "porte-avions") {
			nom = pnom;
			taille = 5;
			detruit = false;
		} else if (pnom == "croiseur") {
			nom = pnom;
			taille = 4;
			detruit = false;
		} else if (pnom == "contre-torpilleur") {
			nom = pnom;
			taille = 3;
			detruit = false;
		} else if (pnom == "sous-marin") {
			nom = pnom;
			taille = 3;
			detruit = false;
		} else if (pnom == "torpilleur") {
			nom = pnom;
			taille = 2;
			detruit = false;
		} else {
			throw new BateauException("Erreur lors de la création du bateau\n");
		}
	}

	/**
	 * calcule et met a jour le taux de destruction du bateau
	 */
	public void pourcentDetruit() {
		int detruitCase = 100 / taille;
		destruction += detruitCase;
		if (destruction >= 99) {
			destruction = 100;
			detruit = true;
		}
	}

	/**
	 * donne l'Etat du bateau
	 * @return etat du bateau
	 */
	public boolean estDetruit() {
		return detruit;
	}

	/**
	 * Comparateur avec la taille des bateaux
	 */
	public static Comparator<Bateau> tailleComparateur = new Comparator<Bateau>() {

		public int compare(Bateau b1, Bateau b2) {
			int bateauTaille1 = b1.getTaille();
			int bateauTaille2 = b2.getTaille();

			return bateauTaille2 - bateauTaille1;
		}
	};

	/**
	 * Comparateur avec le taux de destruction des bateaux
	 */
	public static Comparator<Bateau> destructionComparateur = new Comparator<Bateau>() {

		public int compare(Bateau b1, Bateau b2) {
			int destruc1 = b1.getDestruction();
			int destruc2 = b2.getDestruction();

			return destruc2 - destruc1;
		}
	};
	
	public String getNom() {
		return nom;
	}

	public int getTaille() {
		return taille;
	}

	public int getDestruction() {
		return destruction;
	}
}
