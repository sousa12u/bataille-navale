import java.io.Serializable;

public class Case implements Serializable {
	/**
	 * attribut definissant si un bateau est sur la case ou non
	 */
	private boolean estBateau;
	/**
	 * attribut définissant le bateau sur la case
	 * null s'il n'y a pas de bateau
	 */
	private Bateau bateau;
	/**
	 * attribut définissant si la case est une case de terre
	 */
	private boolean estTerre;
	/**
	 * attribut définissant si la case s'est fait tiré dessus
	 */
	private boolean tire;

	/**
	 * constructeur de Case
	 */
	public Case() {
		estBateau = false;
		bateau = null;
		estTerre = false;
		tire = false;
	}
	
	/**
	 * Constructeur de Case de bateau
	 * @param pBateau bateau a poser sur la case
	 */
	public Case(Bateau pBateau) {
		estBateau = true;
		bateau = pBateau;
		estTerre = false;
		tire = false;
	}
	
	/**
	 * Constructeur de Case de terre
	 * @param pTerre terre a initialiser sur la Case
	 */
	public Case(boolean pTerre) {
		estBateau = false;
		bateau = null;
		estTerre = pTerre;
	}

	public Bateau getBateau() {
		return this.bateau;
	}

	public boolean getEstBateau() {
		return this.estBateau;
	}

	public boolean getEstTerre() {
		return this.estTerre;
	}

	public boolean getTire() {
		return this.tire;
	}

	public void setEstBateau(boolean pEstBateau) {
		this.estBateau = pEstBateau;
	}

	public void setEstTerre(boolean pEstTerre) {
		this.estTerre = pEstTerre;
	}

	public void setTire(boolean pTire) {
		this.tire = pTire;
	}
}
