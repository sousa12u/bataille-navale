import java.io.*;
import java.util.*;

/**
 * Classe Main du jeu
 * @author Alexandre Sousa
 *
 */
public class Main {

	public static void main(String[] args) throws Exception {
		MethodeMain methode = new MethodeMain();
		int tailleGrilleX = 0;
		int tailleGrilleY = 0;
		Scanner sc = new Scanner(System.in);
		Plateau plateau = new Plateau(10, 10);
		plateau.setListeBateau(new ArrayList<Bateau>());
		int choix;
		boolean quitterJeu = false;
		boolean jeufini = false;
		while (!quitterJeu) { // BOUCLE QUI ENVELOPPE TOUT LE JEU
			System.out.println("  MENU :");
			System.out.printf("    1: Nouvelle partie\n    2: Charger\n    0: Quitter\n    Choix : ");
			sc = new Scanner(System.in);
			try {
				choix = sc.nextInt();
			} catch (InputMismatchException e) {
				choix = 111; // erreur de type 111 , aucune partie trouvée
				sc = new Scanner(System.in);
			}
			if (choix == 1) { // CHOIX 1, NOUVELLE PARTIE
				plateau = methode.creerPlateau(tailleGrilleX, tailleGrilleY);
			}
			if (choix == 2) { // CHOIX 2, ON CHARGE UNE PARTIE
				ObjectInputStream ois;
				try {
					ois = new ObjectInputStream(
							new BufferedInputStream(new FileInputStream(new File("sauv/sauvegarder.txt"))));
					plateau = (Plateau) ois.readObject();
					ois.close();
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				} catch (EOFException e) {
					choix = 112;
				}
			}
			if (choix == 0) { // ON QUITTE LE JEU
				quitterJeu = true;
			}
			if (!quitterJeu && choix != 111 && choix != 112) { // SI CHOIX = 1 ou 2 ON LANCE LE JEU
				boolean menu = true;
				boolean tirer = false;
				boolean affBateau = false;
				boolean affDestruction = false;
				boolean sauvegarder = false;
				int sommeBateauDetruit = 0;
				while (menu) {
					sommeBateauDetruit = 0;
					for (Bateau bateau : plateau.getListeBateau()) {
						sommeBateauDetruit += bateau.getDestruction();
					}
					if (sommeBateauDetruit == 500)
						jeufini = true;
					if (jeufini)
						break; // on casse la boucle while du menu si le jeu est fini
					System.out.println(plateau.afficherBateau());
					System.out.println(plateau.afficherTire());
					if (affBateau) {
						affBateau = false;
						methode.affBateau(plateau);
					}
					if (affDestruction) {
						affDestruction = false;
						methode.affDestruction(plateau);
					}
					if (sauvegarder) {
						sauvegarder = false;
						plateau.sauvegarder();
						System.out.println("  Partie sauvegardée !\n");
					}
					System.out.println("  MENU :");
					System.out.print(
							"    1: Tirer\n    2: Afficher les bateaux\n    3: Afficher le taux de destruction des bateaux\n    4: Sauvegarder\n    0: Quitter\n    Choix : ");
					choix = sc.nextInt();
					for (int i = 0; i < 50; ++i)
						System.out.println();
					switch (choix) { // choix réutilisé sans conflit avec le choix du menu 1
					case 0:
						menu = false;
						break;
					case 1:
						tirer = true;
						break;
					case 2:
						affBateau = true;
						break;
					case 3:
						affDestruction = true;
						break;
					case 4:
						sauvegarder = true;
						break;
					default:
						System.out.println("Réponde incorrecte");
					}
					if (tirer) {
						tirer = false;
						methode.tirer(plateau);
					}
				}
				if (jeufini) { // Si le jeu est fini dans la boucle de jeu alors on a gagné et on quitte le jeu
					quitterJeu = true;
				}
			}
			for (int i = 0; i < 50; ++i) // pour clear la console
				System.out.println();
			if (!quitterJeu && choix != 111 && choix != 112) { // si on quitte depuis le menu 1
				System.out.println("Fermeture de la partie ...");
			} else if (!jeufini && quitterJeu && choix != 111 && choix != 112) { //si on quitte depuis le menu 2 puis menu 1
				System.out.println("Fermeture du jeu ...");
			} else if (jeufini && quitterJeu && choix != 111 && choix != 112) { //si on finit le jeu
				for (int i = 0; i < 50; ++i) // pour clear la console
					System.out.println(); 
				System.out.println(plateau.afficherBateau());
				System.out.println(plateau.afficherTire());
				System.out.println("Bravo vous avez gagnés ! (Ou perdus ...)");
				System.out.println("Fermeture du jeu ...");
			} else if (choix == 111) { //si on entre un autre nombre que 0 1 ou 2 dans le menu 1
				System.out.println("Erreur, choix invalide");
			} else if (choix == 112) { //si le fichier de sauvegarde est vide
				System.out.println("\nAucune partie trouvée\n");
			}
		}
	}
}