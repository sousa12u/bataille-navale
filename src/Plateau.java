import java.io.*;
import java.util.ArrayList;
/**
 * Classe qui va créer le plateau et gérer certaines opérations portées dessus
 * @author Alexandre Sousa
 *
 */
public class Plateau implements Serializable {
	/**
	 * grille qui va constituer le plateau
	 */
	private Case grille[][];
	/**
	 * liste des bateaux
	 */
	private ArrayList<Bateau> listeBateau;
	/**
	 * taille X de la grille
	 */
	private int tailleX;
	/**
	 * taille Y de la grille
	 */
	private int tailleY;

	/**
	 * Constructeur de plateau
	 * @param ptx taille X du plateau
	 * @param pty taille Y du plateau
	 */
	public Plateau(int ptx, int pty) {
		if (ptx < 5) {
			ptx = 5;
		}
		if (pty < 5) {
			pty = 5;
		}
		if (ptx > 99) {
			ptx = 99;
		}
		if (pty > 99) {
			pty = 99;
		}
		tailleX = pty;
		tailleY = ptx;

		// Création de la grille
		grille = new Case[pty][ptx];
		for (int j = 0; j < ptx; j++) {
			for (int i = 0; i < pty; i++) {
				grille[i][j] = new Case();
			}
		}

		listeBateau = new ArrayList<Bateau>();
	}
	
	/**
	 * Méthode qui va placer un bateau sur la grille
	 * @param b bateau a placer
	 * @param px position X de la tete du bateau sur la grille
	 * @param py position Y de la tete du bateau sur la grille
	 * @param verticale vrai si le bateau sera a la verticale, faux si le bateau est a l'horizontale
	 * @return vrai si le bateau a été placé, faux si le bateau n'a pas pu être placé
	 * @throws Exception si on sort de la grille 
	 */
	public boolean initPos(Bateau b, int px, int py, boolean verticale) throws Exception {
		boolean res = false;
		boolean estLibre = true;
		px -= 1;
		py -= 1;
		int tailleBateau = b.getTaille();
		try {
			if ((px >= 0 && py >= 0) && (px < tailleY && py < tailleX)) {
				if (verticale) {
					for (int i = py; i < py + tailleBateau; i++) {
						if (grille[i][px].getEstBateau() == true || grille[i][px].getEstTerre())
							estLibre = false;
					}
					if (estLibre) {
						for (int i = py; i < py + tailleBateau; i++) {
							this.grille[i][px] = new Case(b);
						}
						res = true;
					}

				} else {
					for (int i = px; i < px + tailleBateau; i++) {
						if (grille[py][i].getEstBateau() == true || grille[py][i].getEstTerre())
							estLibre = false;
					}
					if (estLibre) {
						for (int i = px; i < px + tailleBateau; i++) {
							grille[py][i] = new Case(b);
						}
						res = true;
					}
				}
			}
		} catch (IndexOutOfBoundsException e) {
			res = false;
		}
		return res;
	}
	
	/**
	 * Méthode qui permet de tirer sur la grille
	 * @param x position X sur la grille
	 * @param y position Y sur la grille
	 * @return vrai si le tire a été efféctué sinon faux
	 */
	public boolean tirerPlateau(int x, int y) {
		boolean res = false;
		x--;
		y--;
		try {
			grille[y][x].setTire(true);
			if (grille[y][x].getEstBateau()) {
				System.out.println("  Touché\n");
				grille[y][x].getBateau().pourcentDetruit();
			} else {
				System.out.println("  Raté\n");
			}
			res = true;
		} catch (IndexOutOfBoundsException e) {
			System.out.println("  Impossible de tirer ici\n");
			res = false;
		}
		return res;
	}

	/**
	 * Méthode qui va permettre de sauvegarder le plateau
	 * dans le fichier sauvegarder.txt qui se trouve dans
	 * le dossier sauv
	 */
	public void sauvegarder() {
		ObjectOutputStream oos;
		try {
			oos = new ObjectOutputStream(
					new BufferedOutputStream(new FileOutputStream(new File("sauv/sauvegarder.txt"))));

			// On ecrit le plateau entier dans le fichier
			oos.writeObject(this);
			oos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Méthode qui permet d'afficher la grille avec les bateaux dessus
	 * @return la chaine de caractère formatée pour afficher la grille
	 */
	public String afficherBateau() {
		String chaine = "    ";
		for (int k = 0; k < tailleY; k++) {
			if (k < 9) {
				chaine += (k + 1) + "  ";
			} else {
				chaine += (k + 1) + " ";
			}
		}
		chaine += "\n";
		for (int j = 0; j < tailleX; j++) {
			if (j < 9) {
				chaine += (j + 1) + "  ";
			} else {
				chaine += (j + 1) + " ";
			}

			for (int i = 0; i < tailleY; i++) {
				chaine += " ";
				if (grille[j][i].getEstBateau() && grille[j][i].getTire()) {
					chaine += "#";
				} else if (grille[j][i].getEstBateau()) {
					chaine += "O";
				} else if (grille[j][i].getEstTerre()) {
					chaine += "+";
				} else {
					chaine += "-";
				}
				chaine += " ";
			}
			chaine += "\n";
		}
		return chaine;
	}

	/**
	 * Méthode qui permet d'afficher la grille avec les tires dessus
	 * @return la chaine de caractère formatée pour afficher la grille
	 */
	public String afficherTire() {
		String chaine = "    ";
		for (int k = 0; k < tailleY; k++) {
			if (k < 9) {
				chaine += (k + 1) + "  ";
			} else {
				chaine += (k + 1) + " ";
			}
		}
		chaine += "\n";
		for (int j = 0; j < tailleX; j++) {
			if (j < 9) {
				chaine += (j + 1) + "  ";
			} else {
				chaine += (j + 1) + " ";
			}

			for (int i = 0; i < tailleY; i++) {
				chaine += " ";
				if (grille[j][i].getTire()) {
					chaine += "X";
				} else {
					chaine += "-";
				}
				chaine += " ";
			}
			chaine += "\n";
		}
		return chaine;
	}
	
	public Case getGrille(int x, int y) {
		return grille[x][y];
	}
	
	public ArrayList<Bateau> getListeBateau() {
		return listeBateau;
	}

	public void setListeBateau(ArrayList<Bateau> listeBateau) {
		this.listeBateau = listeBateau;
	}

	public void addListeBateau(Bateau b) {
		listeBateau.add(b);
	}
}
