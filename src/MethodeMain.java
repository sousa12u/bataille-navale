import java.util.Collections;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Classe qui va regrouper certaines méthodes utilent a la classe Main
 * @author Alexandre Sousa
 *
 */
public class MethodeMain {
	
	/**
	 * Méthode qui s'occupe de créer un plateau de taille variable.
	 * Elle va aussi créer les 5 bateaux et va gérer leur placement
	 * @param tailleGrilleX taille X du plateau
	 * @param tailleGrilleY taille Y du plateau
	 * @return plateau qui va être créer
	 * @throws Exception si les scanner ne sont pas juste
	 */
	public Plateau creerPlateau(int tailleGrilleX, int tailleGrilleY) throws Exception {
		Plateau plateau;
		Scanner sc = new Scanner(System.in);
		// Création de la grille
		System.out.print("  Taille de la grille :\n    X : ");
		tailleGrilleX = sc.nextInt();
		System.out.print("    Y : ");
		tailleGrilleY = sc.nextInt();
		plateau = new Plateau(tailleGrilleX, tailleGrilleY);
		// Création et placement des bateaux
		Bateau b1 = new Bateau("porte-avions");
		Bateau b2 = new Bateau("croiseur");
		Bateau b3 = new Bateau("contre-torpilleur");
		Bateau b4 = new Bateau("sous-marin");
		Bateau b5 = new Bateau("torpilleur");
		System.out.println("\n  Placez vos bateaux : ");
		plateau.addListeBateau(b1);
		plateau.addListeBateau(b2);
		plateau.addListeBateau(b3);
		plateau.addListeBateau(b4);
		plateau.addListeBateau(b5);
		// pour chaque bateau on va demander son orientation et sa position
		int posX, posY;
		boolean estPlace;
		int orientation = 10;
		boolean verticale = false;
		for (Bateau bateau : plateau.getListeBateau()) {
			estPlace = false;
			orientation = 10;
			System.out.println("\n" + plateau.afficherBateau());
			System.out.print("\n  Entrez l'orientation du " + bateau.getNom() + ":");
			System.out.print("\n    1 pour verticale\n    2 pour horizontale : ");
			while (orientation != 1 && orientation != 2) {
				try {
					orientation = sc.nextInt();
				} catch (InputMismatchException e) {
					orientation = 10;
					sc = new Scanner(System.in);
				}
				if (orientation == 1 || orientation == 2) {
					System.out.println("  Orientation validé");
				} else {
					System.out.println("  Erreur, les coordonnées doivent etre valide\n");
				}
			}
			if (orientation == 1) {
				verticale = true;
			}
			if (orientation == 2) {
				verticale = false;
			}
			System.out.println("\n  Entrez la position pour le " + bateau.getNom() + ":");
			while (!estPlace) {
				try {
					System.out.print("    posX : ");
					posX = sc.nextInt();
					System.out.print("    posY : ");
					posY = sc.nextInt();
					estPlace = plateau.initPos(bateau, posX, posY, verticale);
				} catch (InputMismatchException e) {
					estPlace = false;
					posX = 0;
					posY = 0;
					sc = new Scanner(System.in);
				}
				if (!estPlace) {
					System.out.print("\n  Erreur, les coordonnées doivent être valide\n");
				}
			}
		}
		for (int i = 0; i < 50; ++i)
			System.out.println(); // pour clear la console
		return plateau;
	}
	
	/**
	 * Méthode qui va afficher les bateaux triés selon leur taille
	 * @param plateau plateau possédant les bateaux a trier
	 */
	public void affBateau(Plateau plateau) {
		Collections.sort(plateau.getListeBateau(), Bateau.tailleComparateur);
		System.out.println("  Liste des bateaux : ");
		for (Bateau bateau : plateau.getListeBateau()) {

			System.out.println("     -" + bateau.getNom() + " taille : " + bateau.getTaille());
		}
		System.out.println("\n");
	}
	
	/**
	 * Méthode qui va afficher les bateaux triés selon leur taux de destruction
	 * @param plateau plateau possédant les bateaux a trier
	 */
	public void affDestruction(Plateau plateau) {
		Collections.sort(plateau.getListeBateau(), Bateau.destructionComparateur);
		System.out.println("  Liste des bateaux : ");
		for (Bateau bateau : plateau.getListeBateau()) {

			System.out.println("     -" + bateau.getNom() + " : " + bateau.getDestruction() + "% détruit");
		}
		System.out.println("\n");
	}
	
	/**
	 * Méthode qui va gérer la focntionnalité de tire sur la grille
	 * @param plateau plateau contenant la grille
	 */
	public void tirer(Plateau plateau) {
		int posTireX = -5;
		int posTireY = -5;
		Scanner sc = new Scanner(System.in);
		boolean avoirTirer = false;
		while (!avoirTirer) {
			try {
				System.out.println(plateau.afficherTire());
				System.out.print("\n    posX : ");
				posTireX = sc.nextInt();
				System.out.print("    posY : ");
				posTireY = sc.nextInt();
				avoirTirer = plateau.tirerPlateau(posTireX, posTireY);
			} catch (InputMismatchException e) {
				avoirTirer = false;
				System.out.println("\n  Erreur, les coordonnées doivent être valide\\n");
			}
		}
	}
}
