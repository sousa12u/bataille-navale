import static org.junit.Assert.*;

import java.util.Scanner;

import org.junit.Before;
import org.junit.Test;

public class TestJeu {
	Plateau plateau;
	@Before
	public void setUp() {
		plateau = new Plateau(10, 10);
	}
	
	/**
	 * création d'une grille de bateau vide
	 */
	@Test
	public void testCreationGrilleBateau() {
		assertEquals("Devrait etre un plateau de taille 10 sur 10 sans bateau", new Plateau(10, 10).afficherBateau(), plateau.afficherBateau());
	}
	
	/**
	 * création d'une liste de bateau
	 * @throws BateauException 
	 */
	@Test
	public void testCreationListeBateau() throws BateauException {
		plateau.addListeBateau(new Bateau("porte-avions"));
		plateau.addListeBateau(new Bateau("croiseur"));
		assertEquals("Devrait etre une liste de 2 bateaux", 2, plateau.getListeBateau().size());
	}
	
	/**
	 * placement des bateaux sur la grille
	 * @throws Exception 
	 */
	@Test
	public void testPlacementBateau() throws Exception {
		plateau.addListeBateau(new Bateau("porte-avions"));
		plateau.addListeBateau(new Bateau("croiseur"));
		boolean estPlace;
		int i = 2;
		for (Bateau bateau : plateau.getListeBateau()) {
			estPlace = plateau.initPos(bateau, i, i, false);
			i += 4;
		}
		assertEquals("la case[1][1] devrait avoir un bateau",  true, plateau.getGrille(1,1).getEstBateau());
		assertEquals("la case[8][8] devrait ne pas avoir de bateau", false, plateau.getGrille(9, 9).getEstBateau());
	}
	
	/**
	 * si le bateau est détruit
	 * @throws Exception 
	 */
	@Test
	public void testDestructionBateau() throws Exception {
		plateau.addListeBateau(new Bateau("porte-avions"));
		plateau.addListeBateau(new Bateau("croiseur"));
		Boolean estPlace;
		int i = 2;
		for (Bateau bateau : plateau.getListeBateau()) {
			estPlace = plateau.initPos(bateau, i, i, false);
			i += 4;
		}
		Bateau b2 = plateau.getListeBateau().get(1);
		b2.pourcentDetruit();
		b2.pourcentDetruit();
		b2.pourcentDetruit();
		b2.pourcentDetruit();
		assertEquals("le bateau ne devrait pas etre détruit",  false, plateau.getGrille(1,1).getBateau().estDetruit());
		assertEquals("le bateau  devrait  etre détruit",  true, plateau.getGrille(5,5).getBateau().estDetruit());
		assertEquals("le bateau ne devrait pas etre détruit",  0, plateau.getGrille(1,1).getBateau().getDestruction());
		assertEquals("le bateau  devrait  etre détruit",  100, plateau.getGrille(5,5).getBateau().getDestruction());
	}

}














